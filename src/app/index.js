'use strict';

angular.module('onlineShop', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ngRoute', 'ui.bootstrap', 'underscore'])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'productsCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
;
