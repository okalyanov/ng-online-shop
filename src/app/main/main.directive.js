'use strict';

angular.module('onlineShop')
  .directive('scrollPosition', function($window) {
    return {
      scope: {
        scroll: '=scrollPosition'
      },
      link: function(scope) {
        var windowEl = $($window);
        var handler = function() {
          scope.scroll = windowEl.scrollTop();
        };
        windowEl.on('scroll', scope.$apply.bind(scope, handler));
        handler();
      }
    };
  })

  .directive('priceSlider', function() {
    return {
      restrict: 'E',
      template: '<div id="range-slider"></div>',

      link: function(scope, el) {
        var sliderEl = el.find('#range-slider'),
            price = scope.filters.price;

        sliderEl.slider({
          min: price.min,
          max: price.max,
          values: price.val,
          range: true,
          step: 1,

          slide: function(e, ui) {
            scope.$apply(function() {
              price.val = ui.values;
            });
          },

          stop: function(e, ui) {
            scope.$apply(function() {
              price.val = ui.values;
            });
          }
        });

        scope.$watch('filters.price', function(newVal, oldVal) {
          if (newVal !== null && newVal !== undefined) {
            if (newVal.max !== oldVal.max && newVal.max !== sliderEl.slider('option', 'max')) {
              sliderEl.slider('option', 'max', newVal.max);
            }
            if (newVal.min !== oldVal.min && newVal.min !== sliderEl.slider('option', 'min')) {
              sliderEl.slider('option', 'min', newVal.min);
            }
            if (!(angular.equals(newVal.val, oldVal.val)) && !(angular.equals(newVal.val, sliderEl.slider('option', 'values')))) {
              sliderEl.slider('option', 'values', newVal.val);
            }
          }
        }, true);        
      }
    };
  });