'use strict';

describe('onlineShop filters', function() {

  beforeEach(module('onlineShop'));

  // Test cut filter
  describe('cut', function() {

    it('should return cutted string',
        inject(function(cutFilter) {
      
      var result = cutFilter('Lorem ipsum dolor sit amet, consectetur adipiscing elit.', true, 12, '...');

      expect(result).toBe('Lorem ipsum...');
    }));
  });

  // Test unique filter
  describe('unique', function() {

    it('should return only uniques product properties list',
        inject(function(uniqueFilter) {

      var input = uniqueFilter([
            { prop: 'prop_1' },
            { prop: 'prop_1' },
            { prop: 'prop_2' },
            { prop: 'prop_2' },
            { prop: 'prop_1' },
            { prop: 'prop_3' }
          ], 'prop'),

          result = [
            { prop: 'prop_1' },
            { prop: 'prop_2' },
            { prop: 'prop_3' }
          ];

      expect(input).toEqual(result);
    }));
  });

  // Test priceRange filter
  describe('priceRange', function() {

    it('should filter products by price range',
        inject(function(priceRangeFilter) {
      
      var input = [
            { price: 1 },
            { price: 10 },
            { price: 99.99 },
            { price: 100 },
            { price: 115 },
            { price: 230 }
          ];

      expect(priceRangeFilter(input, [10, 99.99]).length).toBe(2);
      expect(priceRangeFilter(input, [99.99, 0]).length).toBe(4);
      expect(priceRangeFilter(input, [0, 99.99]).length).toBe(3);
      expect(priceRangeFilter(input, [0, 0]).length).toBe(6);
    }));
  });

  // Test filterByProps filter
  describe('filterByProps', function() {

    it('should filter products by price range',
        inject(function(filterByPropsFilter) {
      
      var input = [
            { prop: 'prop_1' },
            { prop: 'prop_1' },
            { prop: 'prop_2' },
            { prop: 'prop_2' },
            { prop: 'prop_1' },
            { prop: 'prop_3' }
          ];

      expect(filterByPropsFilter(input, []).length).toBe(6);
      expect(filterByPropsFilter(input, ['prop_1']).length).toBe(3);
      expect(filterByPropsFilter(input, ['prop_1', 'prop_2']).length).toBe(5);
      expect(filterByPropsFilter(input, ['prop_1', 'prop_3', 'prop_2']).length).toBe(6);
    }));
  });
});