'use strict';

describe('onlineShop controllers', function(){
  
  beforeEach(function(){
    jasmine.addMatchers({
      toEqualData: function() {
        return {
          compare: function(actual, expected) {
            return {pass: angular.equals(actual, expected)};
          }
        };
      }
    });
  });

  beforeEach(module('onlineShop'));

  describe('productsCtrl', function(){
    var scope, ctrl, $httpBackend;

    beforeEach(inject(function(_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;
      $httpBackend
          .expectGET('assets/products/products.json')
          .respond([{name: 'prod_1'}, {name: 'prod_2'}, {name: 'prod_3'}, {name: 'prod_4'}]);

      scope = $rootScope.$new();
      ctrl = $controller('productsCtrl', {$scope: scope});
    }));

    it('should create "products" model with 4 products fetched from xhr', function() {
      expect(scope.products).toEqualData([]);

      $httpBackend.flush();

      expect(angular.isArray(scope.products)).toBeTruthy();
      expect(scope.products).toEqualData([{name: 'prod_1'}, {name: 'prod_2'}, {name: 'prod_3'}, {name: 'prod_4'}]);
    });

    it('should set pager', function() {
      expect(scope.pageSize).toBe(3);
      expect(scope.curPage).toBe(1);
      expect(scope.productsCount).toBe(0);

      $httpBackend.flush();

      expect(scope.productsCount).toBe(4);
      expect(scope.noOfPages).toBe(2);
    });

    it('should set property filter', function() {
      expect(scope.filters.props).toEqual([]);
      expect(scope.filters.price).toEqual({
        min: 0,
        max: 0,
        val: [0, 0]
      });

      scope.filters.addProp('prop_1');
      scope.filters.addProp('prop_1');
      scope.filters.addProp('prop_1');
      scope.filters.addProp('prop_2');
      scope.filters.addProp('prop_2');
      scope.filters.addProp('prop_3');

      expect(scope.filters.props).toEqual(['prop_1', 'prop_3']);
    });

    it('should add product to cart', function() {
      expect(scope.cart.items).toEqual([]);

      scope.cart.add({name: 'prod_1'});
      scope.cart.add({name: 'prod_1'});
      scope.cart.add({name: 'prod_2'});
      scope.cart.add({name: 'prod_3'});

      expect(scope.cart.items.length).toBe(4);
    });
  });
});
