'use strict';

angular.module('onlineShop')
  // Slice array from start position
  .filter('startFrom', function() {
    return function(input, start) {
      start = +start; // psarse to int
      return input.slice(start);
    };
  })

  // Cut strings
  .filter('cut', function () {
    return function (value, wordwise, max, tail) {
      if (!value) { return ''; }

      max = parseInt(max, 10);

      if (!max) { return value; }
      
      if (value.length <= max) { return value; }

      value = value.substr(0, max);
      
      if (wordwise) {
        var lastspace = value.lastIndexOf(' ');
        if (lastspace !== -1) {
          value = value.substr(0, lastspace);
        }
      }

      return value + (tail || '…');
    };
  })  

  // Return unique products properties list
  .filter('unique', function() {
    return function(input, key) {
      var unique = {};
      var uniqueList = [];
      for(var i = 0; i < input.length; i++){
        if(typeof unique[input[i][key]] === 'undefined'){
          unique[input[i][key]] = '';
          uniqueList.push(input[i]);
        }
      }
      return uniqueList;
    };
  })

  // Filter products array by checked properties
  .filter('filterByProps', function() {
    return function(products, props) {
      var filtered = [];

      if (props.length > 0) {
        for(var i = 0; i < products.length; i++){
          if ($.inArray(products[i].prop, props) > -1) {
            filtered.push(products[i]);
          }
        }
        return filtered;
      } else {
        return products;
      }
    };
  })

  // Filter products array by price range 
  .filter('priceRange', function() {
    return function(products, price) {
      var filtered = [];

      for (var i = 0; i < products.length; i++) {
        if ( !(price[0] > 0 && price[0] > products[i].price) && !(price[1] > 0 && price[1] < products[i].price) ) {
          filtered.push(products[i]);
        }            
      }
      
      return filtered;
    };
  });



