'use strict';

angular.module('onlineShop')
  .controller('productsCtrl', ['$scope', '_', 'priceRangeFilter', 'filterByPropsFilter', 'productsSrv', function ($scope, _, priceRangeFilter, filterByPropsFilter, productsSrv) {

    $scope.scroll = 0;

    // Init filter object
    $scope.filters = {
      props: [],
      price: {
        min: 0,
        max: 0,
        val: [0, 0]
      },

      // Add checked property to props array
      addProp: function(prop) {
        var i = $.inArray(prop, this.props);
        if (i > -1) {
          this.props.splice(i, 1);
        } else {
          this.props.push(prop);
        }
      }
    };

    // Get products, and set pager
    $scope.pageSize = 3;
    $scope.curPage = 1;
    $scope.products = [];
    $scope.productsCount = 0;

    // $http.get('assets/products/products.json').success(function(data, status, headers, config) {
    $scope.products = productsSrv.query(function(products) {
      $scope.productsCount = products.length;
      $scope.noOfPages = Math.ceil($scope.productsCount/$scope.pageSize);      

      // Get min and max product prices
      var min = Math.floor(parseFloat(_.min(products, function(prod) { return parseFloat(prod.price); }).price)),
          max = Math.ceil(parseFloat(_.max(products, function(prod) { return parseFloat(prod.price); }).price));

      $scope.filters.price = {
        min: min,
        max: max,
        val: [min, max]
      };
    });

    // Watch filters to update pagination
    $scope.$watch('filters', function (newVal, oldVal) {
      if ( !(angular.equals(newVal, oldVal)) ) {
        // Filter products by checked properties and price range
        $scope.filtered = priceRangeFilter(filterByPropsFilter($scope.products, newVal.props), newVal.price.val);

        // Refresh pager parameters
        $scope.productsCount = $scope.filtered.length;
        $scope.noOfPages = Math.ceil($scope.productsCount/$scope.pageSize);
        $scope.curPage = 1;
      }
    }, true);


    // Init cart object
    $scope.cart = {
      items: [],
      add: function(item) {
        this.items.push(item);
        alert('Greeting!' + item.name + 'was added to cart :)');
      },
      buy: function(item) {
        alert('Open the checkout form for the ' + item.name);
      }
    };
  }]);