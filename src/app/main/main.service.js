'use strict';

angular.module('onlineShop')
  .factory('productsSrv', ['$resource', function($resource){
    return $resource('assets/products/:prodFile.json', {}, {
      query: {method:'GET', params:{prodFile:'products'}, isArray:true}
    });
  }]);