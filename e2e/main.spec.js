'use strict';

describe('The main view', function () {
  var page;

  beforeEach(function () {
    browser.get('http://localhost:3000/index.html');
    page = require('./main.po');
  });

  it('list 3 products', function () {
    expect(page.prodsR.count()).toBe(3);
  });

});
