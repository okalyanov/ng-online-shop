# README #

## Installation

1. Download & install [node.js](https://nodejs.org/download/ "Download nodejs")

2. Install the required tools: `yo`, `gulp`, `bower`

```
npm install -g yo gulp bower
```

3. Navigate to project root folder and run

```
npm install && bower install
```

## Use Gulp tasks

* `gulp` or `gulp build` to build an optimized version of your application in `/dist`
* `gulp serve` to launch a browser sync server on your source files
* `gulp serve:dist` to launch a server on your optimized application
* `gulp test` to launch your unit tests with Karma
* `gulp test:auto` to launch your unit tests with Karma in watch mode
* `gulp protractor` to launch your e2e tests with Protractor
* `gulp protractor:dist` to launch your e2e tests with Protractor on the dist files